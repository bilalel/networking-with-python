import dns.resolver
import dns.reversename
import socket

def dig_ptr(ip):
    no = dns.reversename.from_address(ip)
    try:
        answers = dns.resolver.query(no, 'PTR')
        for rdata in answers:
            return str(rdata.target)
    except dns.resolver.NoAnswer:
        print "*** No PTR record for", ip, "***"
    except dns.resolver.NXDOMAIN:
        print "*** No PTR record present ***"


def main():
    hosts = ["213.186.33.4", "46.119.207.14", "37.215.79.184", "84.22.41.101", "95.132.78.60", "91.183.35.171"]

    for host in hosts:
        print "PTR results for ", host, " : ", dig_ptr(host)

if __name__ == '__main__':
    main()
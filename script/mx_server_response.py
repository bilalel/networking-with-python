import socket

def mail_server_present(ip):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(0.5)
    try:
        result = s.connect_ex((ip, 25))
        if result == 0:
            print ("Got a response from a mailserver with ip : " + ip)
            s.close 
            return True
        else:
            print ("Didn't get a response of mailserver with ip : " + ip)
            s.close
            return False
    except:
        print ("Can't get a response of mailserver with ip : " + ip)
        s.close
        return False

def main():
    ip = "91.183.82.112"
    list_ip = ["91.183.82.112" , "37.215.79.184", "84.22.41.101", "95.132.78.60", "91.183.216.35", "95.58.182.191", "91.183.54.204"]
    for ip in list_ip:
        mail_server_present(ip)

if __name__ == '__main__':
    main()

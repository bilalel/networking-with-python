# coding: utf8
#from gluon import *

# Step 1 : Tell if an IP is in blacklist or not
# [ ] Answer dig reverserip.rbl.skynet.be => exist = blacklisted
# [ ] Answer dig reverseip.mmrbl.... => exist = blacklisted

# Step 2 : Cust gives domain
# dig mx domain
# take response with priority 10
# dig response10
# dig reverse-ip.rbl.skynet.be

# Example
# 217.136.252.114 - 114.252.136.217.rbl.skynet.be ==> blacklisted
# 91.183.98.213 - 213.98.183.91.rbl.skynet.be ==> not blacklisted

import dns.resolver
import socket

def reverse_ip(ip):
    new_ip = ip.split('.')
    new_ip.reverse()
    new_ip = '.'.join(new_ip)
    return new_ip

def is_ip(addr):
    """
    Verify if the given ip is legal
    """
    try:
        socket.inet_aton(addr)
        # legal
        return True
    except socket.error:
        # Not legal
        return False

def ip_rbl(ip):
    result = reverse_ip(ip) + ".rbl.skynet.be"
    return result

def ip_mmrbl(ip):
    result = reverse_ip(ip) + ".mmrbl.secure-mail.be"
    return result

def get_ip_mail_server(domain):
    """
    Return IP of the +highest_priority mail server
    """
    mx_answer = dns.resolver.query(domain, 'MX')

    for rdata in mx_answer:
        if rdata.preference == 1:
            mx = rdata.exchange
            break
        elif rdata.preference == 5:
            mx = rdata.exchange
            break
        elif rdata.preference == 10:
            mx = rdata.exchange
            break
        elif rdata.preference == 100:
            mx = rdata.exchange
            break
        else:
            pass
    # retrive ip of the mail server
    ip = dig(mx)
    return ip

def get_host_mail_server(domain):
    """
    Return CNAME of the +highest_priority mail server 
    Using this function to verify if mx server is relay skynet
    """
    mx_answer = dns.resolver.query(domain, 'MX')
    # retrieve mx answer with prior 10
    for rdata in mx_answer:
        if rdata.preference == 1:
            mx = rdata.exchange
            break
        elif rdata.preference == 5:
            mx = rdata.exchange
            break
        elif rdata.preference == 10:
            mx = rdata.exchange
            break
        elif rdata.preference ==100:
            mx = rdata.exchange
            break
        else:
            pass
    return str(mx)

def dig(host):
    try:
        answers = dns.resolver.query(host)
        for rdata in answers:
            return str(rdata)
    except dns.resolver.NoAnswer:
        print "*** No AAAA record for", host, "***"
    except dns.resolver.NXDOMAIN:
        print "*** The name", host, "does not exist ***"

def is_in_blacklist(ip):
    if dig(ip_rbl(ip)) =='127.0.0.1':
        print "The IP ", ip ," is blacklisted by Proximus"
        return True
    elif dig(ip_mmrbl(ip)) =='127.0.0.1':
        print "The IP ", ip ," is blacklisted by Proximus"
        return True
    else:
        print "The IP ", ip ," is not blacklisted by Proximus"
        return False

def is_rbl_blacklist(ip):
    if dig(ip_rbl(ip)) =='127.0.0.1':
        print "The IP ", ip ," is blacklisted(RBL) for spam abuse."
        return True
    else:
        return False

def is_mmrbl_blacklist(ip):
    if dig(ip_mmrbl(ip)) =='127.0.0.1':
        print "The IP ", ip ," is blacklisted(MMRBL) for massive mailing."
        return True
    else:
        return False

def get_domain_reputation(ip):
    pass

def get_ip_reputation(ip):
    domain = reverse_ip(ip) + ".rf.senderbase.org"
    answers = dns.resolver.query(domain, 'TXT')
    result = None
    
    if len(answers) == 1:
        for rdata in answers:
            for txt_string in rdata.strings:
                result = txt_string
    return result

def get_ptr(domain):
    pass

def check_blacklist(host):
    all_data ={
        'given': host,
        'ip':'',
        'domain':'',
        'mailserver_domain':'',
        'mailserver_ip':'',
        'ip_reputation': None,
        'domain_reputation': None,
        'ptr': None,
        'mailserver_skynet':False,
        'rbl_listed': False,
        'mmrbl_listed': False,
        'blacklisted':False
    }
    if is_ip(host):
        all_data['ip'] = host
        all_data['rbl_listed'] = is_rbl_blacklist(host)
        all_data['mmrbl_listed'] = is_mmrbl_blacklist(host)
        all_data['ip_reputation'] = get_ip_reputation(host)
        if all_data['rbl_listed'] or all_data['mmrbl_listed']:
            all_data['blacklisted'] = True
    else:
        all_data['domain'] = host
        if get_host_mail_server(host) == "in.mx.skynet.be.":
            all_data['mailserver_skynet'] = True
            print "Mail server hosted on skynet server. Please ask the user the ip adress of his connection,"
            print"and launch a new blacklist test for this IP."
        else:
            all_data['domain'] = host
            all_data['mailserver_domain'] = get_host_mail_server(host)
            all_data['mailserver_ip'] = get_ip_mail_server(host)
            all_data['ptr'] = get_ptr(host)
            all_data['rbl_listed'] = is_rbl_blacklist(host)
            all_data['mmrbl_listed'] = is_mmrbl_blacklist(host)
            all_data['domain_reputation'] = get_domain_reputation(host)
            all_data['domain_reputation'] = get_ip_reputation(all_data['mailserver_ip'])
            if all_data['rbl_listed'] or all_data['mmrbl_listed']:
                all_data['blacklisted'] = True
    print all_data
    return all_data



# ----------------------

def test_dig():
    host = "google.be"
    host = "114.252.136.217.rbl.skynet.be"
    host = "213.98.183.91.rbl.skynet.be"
    host = "nukkiefun.be"
    print dig(host)
    #print('Reverse of IP 91.183.98.213 is ' + reverse_ip('91.183.98.213'))
    #print('rbl host of 91.183.98.213 is ' + ip_rbl('91.183.98.213'))
    #print('mmrbl host of IP 91.183.98.213 is ' + ip_mmrbl('91.183.98.213'))
    #is_ip("91.183.12.34")
    #is_ip("lan")
    #print "ip of mail server ", host, " is ", dig(host)
    #print "server mail of nukkiefun.be is : " , get_ip_mail_server("nukkiefun.be")

def test_tld():
    host = "mail.nukkiefun.be"

    temp_host = "http://" + host
    res = get_tld(temp_host, as_object=True)
    print res
    # 'nukkiefun.be'

    print res.subdomain
    # 'mail'

    print res.domain
    # 'nukkiefun'

    print res.suffix
    # 'be'

    print res.tld
    # 'nukkiefun.be'

def test_txt():
    ip = "195.238.6.51"
    get_ip_reputation(ip)

def test():
    """
    if input is IP : check if blacklisted
    if input is domain : retrieve the ip of mailserver and check if blacklisted
    """
    host = "nukkiefun.be"
    host = "nukkiefun.net"
    #host = "195.238.2.21"
    #host = "217.136.252.114"
    if is_ip(host):
        ip = host
        is_in_blacklist(host)
    else:
        if get_host_mail_server(host) == "in.mx.skynet.be.":
            # This part has to be tested.
            print "Mail server hosted on skynet server. Please ask the user the ip adress of his connection,"
            print"and launch a new blacklist test for this IP."
        else:
            ip =  get_ip_mail_server(host)
            is_in_blacklist(ip)

def main():
    host = "nukkiefun.be"
    #host = "nukkiefun.net"
    #host = "195.238.2.21"
    #host = "217.136.252.114"
    host = "194.78.225.146"

    print "The given host is : ", host
    check_blacklist(host)

if __name__ == "__main__":
    main()
    #test()
    #test_dig()
    #test_tld()
    #test_txt()
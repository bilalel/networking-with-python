import re

pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

str_true = ('test@mail.com',)
            
str_false = ('testmail.com', '@testmail.com', 'test@mailcom')


mails = ["bilal.el@gmail.com", "bilal.com", "123@it.com", "123@123.1"]

for t in str_true:
    assert(bool(re.match(pattern, t)) == True), '%s is not True' %t
for f in str_false:
    assert(bool(re.match(pattern, f)) == False), '%s is not False' %f


for mail in mails:
    if re.match(pattern, mail):
        print '%s is True' %mail
    else:
        print '%s is False' %mail
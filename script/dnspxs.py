#!/usr/bin/env python

import operator
import re

import dns.resolver
import dns.reversename
import socket

class Mailobj(object):
    """docstring for Mail"""
    def __init__(self, ip=None, hostname=None, preference=None):
        self.preference = preference
        self.hostname = hostname
        self.ip = ip
        self.reversed_ip = None
        self.domain = None
        self.ip_reputation = None
        self.domain_reputation = None
        self.ptr = None
        self.response_mail_server = None
        self.rbl_listed = False
        self.mmrbl_listed = False
        self.rbl_host = None
        self.mmrbl_host = None
        self.blacklisted = False

        if self.ip:
            # If IP retrieved, then hostname wasn't given.
            self.reverse_ip()
            self.is_rbl_blacklist()
            self.is_mmrbl_blacklist()
            self.is_blacklisted()
            self.get_ip_reputation()
            self.dig_ptr()
            self.mail_server_present()
            
    def to_dict(self):
        return self.__dict__

    # helper methods
    def reverse_ip(self):
        new_ip = self.ip.split('.')
        new_ip.reverse()
        new_ip = '.'.join(new_ip)
        self.reversed_ip = new_ip 

    def dig(self, host):
        try:
            answers = dns.resolver.query(host)
            for rdata in answers:
                return str(rdata)
        except dns.resolver.NoAnswer:
            # "*** No AAAA record for", host, "***"
            pass
        except dns.resolver.NXDOMAIN:
            # "*** The name", host, "does not exist ***"
            pass

    def dig_ptr(self):
        no = dns.reversename.from_address(self.ip)
        try:
            answers = dns.resolver.query(no, 'PTR')
            for rdata in answers:
                self.ptr = str(rdata.target)
        except dns.resolver.NoAnswer:
            # "*** No PTR record for", ip, "***"
            self.ptr = None
        except dns.resolver.NXDOMAIN:
            # "*** No PTR record present ***"
            self.ptr = None
    # [end] helper methods

    # main methods
    def is_rbl_blacklist(self): # done
        self.rbl_host = self.reversed_ip + ".rbl.skynet.be"
        if self.dig(self.rbl_host) =='127.0.0.1':
            # The IP is blacklisted(RBL) for spam abuse.
            self.rbl_listed = True
        else:
            self.rbl_listed = False

    def is_mmrbl_blacklist(self): # done
        self.mmrbl_host = self.reversed_ip + ".mmrbl.secure-mail.be"
        if self.dig(self.mmrbl_host) =='127.0.0.2':
            # The IP is blacklisted(MMRBL) for massive mailing.
            self.mmrbl_listed = True
        else:
            self.mmrbl_listed = False
    
    def get_ip_reputation(self): # done
        domain = self.reversed_ip + ".rf.senderbase.org"
        result = None
        try:
            answers = dns.resolver.query(domain, 'TXT')
            if len(answers) == 1:
                for rdata in answers:
                    for txt_string in rdata.strings:
                        result = txt_string
            self.ip_reputation = result
        except:
            self.ip_reputation = result

    def mail_server_present(self): # done
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(0.5)
        try:
            result = s.connect_ex((self.ip, 25))
            if result == 0:
                #"Got a response from a mailserver
                s.close 
                self.response_mail_server = True
            else:
                #"Didn't get a response of mailserver"
                s.close
                self.response_mail_server =  False
        except:
            #"Can't get a response of mailserver"
            s.close
            self.response_mail_server =  False

    def is_blacklisted(self):
        if self.rbl_listed or self.mmrbl_listed:
            self.blacklisted = True
        else:
            self.blacklisted = False
    # [end] main methods
    

    # All get and assign methods
    def ip(self, ip=None):
        if ip==None:
            return self.ip
        else:
            self.ip=ip
    
    def host(self, host=None):
        if host==None:
            return self.host
        else:
            self.host=host

    def domain(self, domain=None):
        if domain==None:
            return self.domain
        else:
            self.domain=domain

    def preference (self, preference=None):
        if preference==None:
            return self.preference
        else:
            self.preference=preference

    def rbl_host(self, rbl_host=None):
        if rbl_host == None:
            return self.rbl_host
        else:
            self.rbl_host = rbl_host

    def mmrbl_host(self, mmrbl_host=None):
        if mmrbl_host == None:
            return self.mmrbl_host
        else:
            self.mmrbl_host = mmrbl_host

    def rbl_listed(self, rbl_listed=None):
        if rbl_listed==None:
            return self.spam_abuse
        else:
            self.rbl_listed=rbl_listed

    def mmrbl_listed(self, mmrbl_listed=None):
        if mmrbl_listed==None:
            return self.mmrbl_listed
        else:
            self.mmrbl_listed=mmrbl_listed

    def blacklisted(self, blacklisted=None):
        if blacklisted==None:
            return self.blacklisted
        else:
            self.blacklisted=blacklisted

    def ip_reputation(self, ip_reputation=None):
        if ip_reputation==None:
            return self.ip_reputation
        else:
            self.ip_reputation=ip_reputation

    def domain_reputation(self, domain_reputation=None):
        if domain_reputation==None:
            return self.domain_reputation
        else:
            self.domain_reputation=domain_reputation

    def ptr(self, ptr=None):
        if ptr==None:
            return self.ptr
        else:
            self.ptr=ptr

    def response_mail_server(self, response_mail_server=None):
        if response_mail_server==None:
            return self.response_mail_server
        else:
            self.ptr=response_mail_server


    # [end] All get and assign methods


class Custobjct(object):
    """docstring for Domain"""
    def __init__(self, host):
        self.host = host # Is it necessary?
        self.ip = None
        self.domain = None
        self.email = None
        self.is_valid_ip = None
        self.is_valid_domain = None
        self.is_valid_email = None
        self.is_skynet = None
        self.mail_server_as_list= []

        self.is_ip_or_domain()
        self.is_email()
        if self.domain:
            self.get_host_mail_server()
            self.is_skynet_server()
        elif self.is_valid_email:
            self.get_host_mail_server()
            self.is_skynet_server()
        elif self.ip:
            self.mail_server_as_list = [Mailobj(self.ip).to_dict()]
        else:
            print "Given info isn't an IP nor a domain nor an email"


    def to_dict(self):
        return self.__dict__

    def is_email(self):
        """ verify if given input is email, then retrieve the domain name."""
        pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"

        if re.match(pattern, self.host):
            self.email = self.host
            self.is_valid_email = True
            self.domain = self.email.split("@")[1]
        else:
            self.is_valid_email = False

    def is_ip_or_domain(self): # todo -> fine tunning domain
        if self.is_valid_ipv4_address(self.host):
            self.ip = self.host
            self.is_valid_ip = True
        else:
            self.domain = self.host
            self.is_valid_domain = True

    def is_valid_ipv4_address(self, address):
        try:
            socket.inet_pton(socket.AF_INET, address)
        except AttributeError:  # no inet_pton here, sorry
            try:
                socket.inet_aton(address)
            except socket.error:
                return False
            return address.count('.') == 3
        except socket.error:  # not a valid address
            return False

        return True

    def is_skynet_server(self):
        """
        When the list of mx servers is generated, check if mx record with
        preference 10 is a skynet mail server.
        """
        str_skynet = "in.mx.skynet.be."
        if self.mail_server_as_list[0]["hostname"] == str_skynet:
            self.is_skynet = True
        else:
            self.is_skynet = False

    def dig(self, host):
        try:
            answers = dns.resolver.query(host)
            for rdata in answers:
                return str(rdata)
        except dns.resolver.NoAnswer:
            # "*** No AAAA record for", host, "***"
            pass
        except dns.resolver.NXDOMAIN:
            # "*** The name", host, "does not exist ***"
            pass

    def get_host_mail_server(self): # [x] used only when domain given
        """
        Retrieve all recored mx servers (hostname + preference).
        Then retrieve the ip recorded for the hostname.
        Finally starts all basical check (rbl, server response, ptr, ...)
        """
        try:
            mx_answer = dns.resolver.query(self.domain, 'MX')
            mailservers=[rdata.to_text() for rdata in mx_answer] 
            # sort wasn't doing the job
            # mailservers.sort()

            # split returns a list, I needed two string
            # mailservers = [Mailobj(mailserver.split(' '))for mailserver in mailservers]

            # I split the preference and the hostname
            # Then add ip of each host by mailserver
            # result : [[a, b, c], [a,b, c], ...]
            # a, b, c = preference, hostname, ip

            mailservers = [mailserver.split(' ') for mailserver in mailservers]
            for mailserver in mailservers:
                mailserver.append(self.dig(mailserver[1]))
            mailservers = [Mailobj(mailserver[2], mailserver[1], mailserver[0]).to_dict() for mailserver in mailservers]
            mailservers = sorted(mailservers, key=lambda x: int(operator.itemgetter("preference")(x)))
            self.mail_server_as_list = mailservers

        except Exception, e:
            print "Erreur : %s" % e
            self.mail_server_as_list = []

# ================================

def main():
    ip = "194.78.225.146"
    ip = "217.136.252.114"
    ip = "95.58.182.191"
    ip = "91.183.54.204" # mass mailing
    hostname = "mx1.ovh.net."

    customer = Mailobj(None, hostname)
    print "The ip is : ", customer.ip
    print "The reversed ip is : ", customer.reversed_ip
    print "The ip is rbl listed", customer.rbl_listed
    print "The ip is mmrbl listed",customer.mmrbl_listed
    print "The ip is blacklisted", customer.blacklisted
    print "IP reputation: ", customer.ip_reputation
    print "PTR : ", customer.ptr
    print "Mail server response :", customer.response_mail_server
    print customer.to_dict()

def bydomain():
    host = "reno-k.be"
    host = "thesmithfamily.org"
    host = "ben@thesmithfamily.org"
    host = "info@nukkiefun.net"
    #host = "91.183.54.204" 
    check_host = Custobjct(host)
    print check_host.to_dict()

if __name__ == '__main__':
    #main()
    bydomain()